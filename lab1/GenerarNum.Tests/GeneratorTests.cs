using Generators;

namespace GenerarNum.UnitTests.Generators {

    [TestFixture]
    public class RangeGeneratorTests
    {
        private INumGenerator _rangeGenerator;
        private INumGenerator _forGenerator;

        [SetUp]
        public void Setup()
        {
            this._rangeGenerator = new RangeGenerator();
            this._forGenerator = new ForGenerator();            
        }

        private void GenerateOne(INumGenerator generator)
        {
            var nums = generator.GenerateNums(1);
            Assert.AreEqual(nums.Count, 1);
            Assert.AreEqual(nums.ElementAt(0), 1);
        }

        private void GenerateNegative(INumGenerator generator)
        {
            var nums = generator.GenerateNums(-1);
            Assert.AreEqual(nums.Count, 0);
        }

        private void GenerateZero(INumGenerator generator)
        {
            var nums = generator.GenerateNums(0);
            Assert.AreEqual(nums.Count, 0);
        }

        private void GenerateMany(INumGenerator generator)
        {
            var nums = generator.GenerateNums(10);
            Assert.AreEqual(nums.Count, 10);
            Assert.AreEqual(nums.ElementAt(0), 1);
            Assert.AreEqual(nums.ElementAt(4), 5);
            Assert.AreEqual(nums.ElementAt(9), 10);
        }

        [Test]
        public void TestGenerateOne()
        {
            this.GenerateOne(this._forGenerator);
            this.GenerateOne(this._rangeGenerator);
        }

        [Test]
        public void TestGenerateZero()
        {
            this.GenerateZero(this._forGenerator);
            this.GenerateZero(this._rangeGenerator);
        }

        [Test]
        public void TestGenerateNegative()
        {
            this.GenerateNegative(this._forGenerator);
            this.GenerateNegative(this._rangeGenerator);
        }

        [Test]
        public void TestGenerateMany()
        {
            this.GenerateMany(this._forGenerator);
            this.GenerateMany(this._rangeGenerator);
        }
    }
}
