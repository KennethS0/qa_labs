﻿namespace Generators
{

    public class RangeGenerator : INumGenerator 
    {
        public List<int> GenerateNums(int n) 
        {
            List<int> nums;
            if (n >= 1) {
                nums = Enumerable.Range(1, n).ToList();
                
                foreach (var num in nums)
                {
                    Console.WriteLine($"Range: {num}");
                }
            } else {
                nums = new List<int>();
            }

            return nums;
        }   
    }
}
