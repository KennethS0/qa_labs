namespace Generators
{

    public class ForGenerator : INumGenerator 
    {
        public List<int> GenerateNums(int n) 
        {
            List<int> nums = new List<int>();

            for (int i = 1; i <= n; i++) 
            {
                Console.WriteLine($"For: {i}");
                nums.Add(i);
            }
            return nums;
        }   
    }
}
