namespace Generators
{
    public interface INumGenerator
    {
        List<int> GenerateNums(int n);   
    }
}
