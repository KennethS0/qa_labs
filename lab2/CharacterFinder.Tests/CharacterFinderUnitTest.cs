using CharFinder;

namespace CharTests
{

    public class CharacterFinderUnitTests
    {
        private CharacterFinder _charFinder;

        [SetUp]
        public void Setup()
        {
            this._charFinder = new CharacterFinder();
        }

        [Test]
        public void FindPos()
        {
            int indx = this._charFinder.FindCharacter('a', "123a");
            Assert.AreEqual(indx, 3);
        }

        [Test]
        public void EmptyStr()
        {
            int indx = this._charFinder.FindCharacter('a', "");
            Assert.AreEqual(indx, -1);
        }

        [Test]
        public void NotFound()
        {
            int indx = this._charFinder.FindCharacter('b', "aaaaaaaaaaa");
            Assert.AreEqual(indx, -1);
        }
    }
}