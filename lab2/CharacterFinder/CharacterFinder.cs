﻿namespace CharFinder
{
    public class CharacterFinder 
    {   
        public int FindCharacter(char letter, String str)
        {
            int index = -1;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == letter) 
                {
                    index = i;
                    break;
                }
            }
            return index;
        }
    }
}
